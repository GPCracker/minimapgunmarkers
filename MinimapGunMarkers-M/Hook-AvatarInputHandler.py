#*****
# AvatarInputHandler Hooks
#*****
def new_AvatarInputHandler_handleKeyEvent(self, event):
	result = old_AvatarInputHandler_handleKeyEvent(self, event)
	key, isDown, isRepeat, mods = keyEventParse(event)
	modActivationSwitchKey = _config_['activation']['switchKey']
	modActivationKeySwitchMode = _config_['activation']['keySwitchMode']
	MGMControllerLoaded = lambda: hasattr(BigWorld.player(), 'XMinimapGunMarkersController') and BigWorld.player().XMinimapGunMarkersController is not None
	if (key, mods) == parseHotKeySequence(modActivationSwitchKey) and not isRepeat and MGMControllerLoaded():
		if modActivationKeySwitchMode and isDown:
			_config_['activation']['activated'] = not _config_['activation']['activated']
			if _config_['activation']['activated']:
				showMessageOnPanel('PlayerMessagesPanel', 0, _config_['activation']['messageA'], 'green')
			else:
				showMessageOnPanel('PlayerMessagesPanel', 0, _config_['activation']['messageD'], 'red')
		elif not modActivationKeySwitchMode:
			_config_['activation']['activated'] = isDown
		BigWorld.player().XMinimapGunMarkersController.enabled = _config_['activation']['activated']
	for displayFilter in _config_['displayFilters']:
		if not displayFilter['enabled']:
			continue
		if (key, mods) == parseHotKeySequence(displayFilter['switchKey']) and not isRepeat and MGMControllerLoaded():
			if displayFilter['keySwitchMode'] and isDown:
				displayFilter['activated'] = not displayFilter['activated']
			elif not displayFilter['keySwitchMode']:
				displayFilter['activated'] = isDown
			BigWorld.player().XMinimapGunMarkersController.getFilterByTags(displayFilter['tags']).activated = displayFilter['activated']
			BigWorld.player().XMinimapGunMarkersController.onVehicleFiltersChanged()
	return result
