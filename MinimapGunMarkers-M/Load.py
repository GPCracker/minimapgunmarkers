#*****
# Mod loading
#*****
def showStartupMessage(message):
	from gui import SystemMessages
	import functools
	if SystemMessages.g_instance is None:
		BigWorld.callback(1.0, functools.partial(showStartupMessage, message))
	elif message is not None and message != '':
		SystemMessages.pushMessage(message)
	return None

def loadMod():
	global _config_
	_config_ = readConfig()
	if _config_['ignoreClientVersion'] or getClientVersion() == __clientVersion__:
		if _config_['modEnabled']:
			BigWorld.callback(_config_['hookSetTimeout'], injectHooks)
			showStartupMessage(_config_['modLoadedMessage'])
	else:
		showStartupMessage(_config_['modUpdateMessage'])
		print '***** Please update {}! *****'.format(__application__)
	return None

#*****
# Start mod
#*****
loadMod()
