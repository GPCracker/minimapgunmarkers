#*****
# Configuration
#*****
_config_ = None

#*****
# Default configuration
#*****
def defaultConfig():
	return {
		'modEnabled': ('Bool', True),
		'ignoreClientVersion': ('Bool', False),
		'hookSetTimeout': ('Float', 3.0),
		'modLoadedMessage': ('WideString', u'{} loaded.'.format(__application__)),
		'modUpdateMessage': ('WideString', u'Please update {}!'.format(__application__)),
		'activation': {
			'switchKey': ('String', 'KEY_LCONTROL+KEY_M'),
			'keySwitchMode': ('Bool', True),
			'activated': ('Bool', True),
			'messageA': ('WideString', u'Minimap gun markers: ENABLED.'),
			'messageD': ('WideString', u'Minimap gun markers: DISABLED.')
		},
		'displayFilters': ('FiltersList', [
			{
				'enabled': ('Bool', False),
				'switchKey': ('String', 'KEY_NONE'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', True),
				'tags': ('TagsList', ['enemy'])
			},
			{
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_NONE'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', True),
				'tags': ('TagsList', ['ally', 'SPG'])
			},
			{
				'enabled': ('Bool', True),
				'switchKey': ('String', 'KEY_NONE'),
				'keySwitchMode': ('Bool', True),
				'activated': ('Bool', True),
				'tags': ('TagsList', ['squad'])
			}
		])
	}

#*****
# Read configuration from file
#*****
def readConfig():
	mainSection = ResMgr.openSection(__file__.replace('.pyc', '.xml'))
	if mainSection is None:
		print '[{}] Config loading failed.'.format(__appShortName__)
	else:
		print '[{}] Config successfully loaded.'.format(__appShortName__)
	configReader = ConfigReader()
	defaultTag = ''
	defaultFilter = {
		'enabled': ('Bool', False),
		'switchKey': ('String', 'KEY_NONE'),
		'keySwitchMode': ('Bool', True),
		'activated': ('Bool', False),
		'tags': ('TagsList', [])
	}
	tagsFilter = lambda tag: tag in ['ally', 'teamKiller', 'enemy', 'squad', 'lightTank', 'mediumTank', 'heavyTank', 'SPG', 'AT-SPG']
	configReader.extTypes = {
		'TagsList': lambda *args: filter(tagsFilter, configReader.readList(*args, itemName = 'tag', itemType = 'String', itemDefault = defaultTag)),
		'FiltersList': lambda *args: configReader.readList(*args, itemName = 'filter', itemType = 'Filter', itemDefault = defaultFilter),
		'Filter': lambda *args: configReader.readDict(*args)
	}
	return configReader.readSection(mainSection, defaultConfig())
