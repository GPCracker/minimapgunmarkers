#*****
# Injecting Hooks
#*****
def injectHooks():
	from Vehicle import Vehicle
	global old_Vehicle_startVisual
	old_Vehicle_startVisual = Vehicle.startVisual
	Vehicle.startVisual = new_Vehicle_startVisual
	global old_Vehicle_stopVisual
	old_Vehicle_stopVisual = Vehicle.stopVisual
	Vehicle.stopVisual = new_Vehicle_stopVisual
	global old_Vehicle_onVehicleDeath
	old_Vehicle_onVehicleDeath = Vehicle._Vehicle__onVehicleDeath
	Vehicle._Vehicle__onVehicleDeath = new_Vehicle_onVehicleDeath
	from AvatarInputHandler import AvatarInputHandler
	global old_AvatarInputHandler_handleKeyEvent
	old_AvatarInputHandler_handleKeyEvent = AvatarInputHandler.handleKeyEvent
	AvatarInputHandler.handleKeyEvent = new_AvatarInputHandler_handleKeyEvent
	from gui.Scaleform.Minimap import MinimapZIndexManager
	MinimapZIndexManager._GUN_RANGE = (261, 310)
	MinimapZIndexManager.getGunIndex = lambda self, id: self._GUN_RANGE[0] + (self.getVehicleIndex(id) - self._VEHICLE_RANGE[0])
	return None
