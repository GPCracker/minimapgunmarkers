#*****
# Vehicle Hooks
#*****
def new_Vehicle_startVisual(self):
	result = old_Vehicle_startVisual(self)
	if not hasattr(BigWorld.player(), 'XMinimapGunMarkersController') or BigWorld.player().XMinimapGunMarkersController is None:
		getVehicleFilter = lambda displayFilter: VehicleFilter(displayFilter['tags'], displayFilter['enabled'], displayFilter['activated'])
		vehicleFilters = map(getVehicleFilter, _config_['displayFilters'])
		BigWorld.player().XMinimapGunMarkersController = MinimapGunMarkersController(vehicleFilters)
	if hasattr(BigWorld.player(), 'XMinimapGunMarkersController') and BigWorld.player().XMinimapGunMarkersController is not None:
		if self.isAlive():
			BigWorld.player().XMinimapGunMarkersController.onVehicleStart(self.id)
	return result

def new_Vehicle_stopVisual(self):
	if hasattr(BigWorld.player(), 'XMinimapGunMarkersController') and BigWorld.player().XMinimapGunMarkersController is not None:
		BigWorld.player().XMinimapGunMarkersController.onVehicleStop(self.id)
	return old_Vehicle_stopVisual(self)

def new_Vehicle_onVehicleDeath(self, *args, **kwargs):
	if hasattr(BigWorld.player(), 'XMinimapGunMarkersController') and BigWorld.player().XMinimapGunMarkersController is not None:
		BigWorld.player().XMinimapGunMarkersController.onVehicleDeath(self.id)
	return old_Vehicle_onVehicleDeath(self, *args, **kwargs)
