#Disclaimer:
#	This file was written in Russian, and partially translated by special software.
#	Configuration file was translated by author himself in case of preventing data distortion.
#	Author is not responsible for the accuracy of any other translations except Russian below.

Маркеры направления орудий на миникарте / Minimap Gun Markers
Модификация для World of Tanks / Modification for World of Tanks

Автор / Author: GPCracker

[Описание модификации]
Модификация может отображать направление стволов видимой техники в квадрате отрисовки на миникарте. Маркеры отображаются только для техники, прошедшей через фильтры. Например, с помощью фильтров можно добавить маркеры направления орудий для союзной артиллерии, совзводных, или для видимых противников. Для всех этих трех категорий в дефолтном файле конфигурации есть фильтры. Фильтр для врагов по умолчанию выключен. Также вы можете добавлять свои фильтры.

[Важная информация]
Некоторые функции модификации могут нарушать правила игры. Советуем Вам прочитать их перед редактированием файла конфигурации.
Загрузчики скриптов с модификацией не поставляются. Вы ищете и устанавливаете их самостоятельно.
Количество фильтров не ограничено, фильтры с одинаковым набором тегов недопустимы.

[Редактирование файла конфигурации]
Файл конфигурации нельзя редактировать стандартным блокнотом, входящим в Windows, а также другими простыми текстовыми редакторами, не поддерживающими кодировку файла конфигурации - UTF-8 w/o BOM.
Все параметры файла конфигурации имеют комментарии на русском и английском языках. Если у параметра нет комментария, значит этот параметр определен выше. Ищите одноименный параметр / секцию.

[Принцип отбора техники фильтрами]
1. Маркер направления ствола может отображаться только для видимых (отображаемых) танков, находящихся в квадрате отрисовки. Это ограничение движка игры.
2. Маркер отображается только для техники, прошедшей хотя бы через 1 фильтр.
3. Техника проходит через фильтр, если ВСЕ теги фильтра к ней применимы. То есть, если в тегах указано более одного КЛАССА техники, через фильтр не пройдет ничего (танк не может быть легким и тяжелым одновременно). Для каждого класса создается отдельный фильтр. Фильтр, не содержащий ни одного тега, пропускает все.
4. Доступные теги: 'ally', 'enemy', 'squad', 'teamKiller', 'lightTank', 'mediumTank', 'heavyTank', 'SPG', 'AT-SPG'.

[Description of modification]
Modification can display the direction of guns of visible vehicles in a drawing square on the minimap. Markers are displayed only for the vehicles which passed through filters. For example, by means of filters it is possible to add markers of the gun's direction for allied artillery, squadmen, or for visible opponents. For all these three categories in the default configuration file there are filters. The filter for enemies is by default switched off. Also you can add the filters.

[Important information]
Some functions of modification can break rules of the game. We advise you to read them before editing the file of a configuration.
Loaders of scripts with modification aren't delivered. You look for and you establish them independently.
The number of filters isn't limited, filters with an identical set of tags are inadmissible.

[Editing file of a configuration]
It is impossible to edit the configuration file with the standard notepad included in Windows, and also with other simple text editors which aren't supporting the encoding of the configuration file - UTF-8 w/o BOM.
All parameters of the configuration file have comments in the Russian and English languages. If parameter has no comment, this parameter means is determined above. Look for the parameter / section of the same name.

[Principle of selection by vehicle filters]
1. The marker of the gun's direction can be displayed only for the visible (displayed) tanks which are in a drawing square. This is limitation of game engine.
2. The marker is displayed only for the vehicles which passed at least through 1 filter.
3. The vehicle passes through the filter if ALL tags of the filter are applicable to it. That is, if in tags it is specified more than one CLASS of vehicle, won't pass anything through the filter (the tank can't be light and heavy at the same time). For each class the separate filter is created. The filter which isn't containing any tag passes everything.
4. Available tags: 'ally', 'enemy', 'squad', 'teamKiller', 'lightTank', 'mediumTank', 'heavyTank', 'SPG', 'AT-SPG'.
