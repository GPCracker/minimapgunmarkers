class VehicleFilter(object):
	@staticmethod
	def getVehicleTags(vehicleID):
		vehicleTags = set([])
		if isVehicleAlive(vehicleID):
			vehicleTags.add('alive')
		if isVehicleAlly(vehicleID):
			vehicleTags.add('ally')
		if isVehicleTeamKiller(vehicleID):
			vehicleTags.add('teamKiller')
		if isVehicleEnemy(vehicleID):
			vehicleTags.add('enemy')
		if isVehicleSquad(vehicleID):
			vehicleTags.add('squad')
		if isVehiclePlayer(vehicleID):
			vehicleTags.add('player')
		vehicleTags.add(getVehicleClass(vehicleID))
		return frozenset(vehicleTags)
	
	def __init__(self, filterTags, enabled = True, activated = True):
		self.filterTags = frozenset(filterTags)
		self.enabled = enabled
		self.activated = activated
		return None
	
	def __call__(self, vehicleID):
		vehicleTags = self.getVehicleTags(vehicleID)
		return self.enabled and self.activated and vehicleTags & self.filterTags == self.filterTags and vehicleTags & frozenset(['player', 'alive']) == frozenset(['alive'])
	
	def __eq__(self, other):
		return type(self) is type(other) and self.filterTags == other.filterTags
	
	def __ne__(self, other):
		return not self.__eq__(other)
	
	def __hash__(self):
		return hash(self.filterTags)
	
	def __del__(self):
		return None

class MinimapGunMarkersController(object):
	@staticmethod
	def getWGCombinedMP(translationSrc, rotationSrc):
		combinedMP = Math.WGCombinedMP()
		combinedMP.translationSrc = translationSrc
		combinedMP.rotationSrc = rotationSrc
		return combinedMP
	
	@staticmethod
	def getMinimap():
		from gui.WindowsManager import g_windowsManager
		return g_windowsManager.battleWindow.minimap
	
	def __init__(self, vehicleFilters, enabled = True):
		self.__vehicleFilters = set(vehicleFilters)
		self.__enabled = enabled
		self.__visibleVehicles = set()
		self.__minimapEntryManager = MinimapEntryManager()
		return None
	
	@property
	def enabled(self):
		return self.__enabled
	
	@enabled.setter
	def enabled(self, value):
		if value != self.__enabled:
			self.__enabled = value
			self.__updateMarkers()
		return None
	
	def getFilterByTags(self, filterTags):
		vehicleFilters = filter(lambda vehicleFilter: vehicleFilter.filterTags == frozenset(filterTags), self.__vehicleFilters)
		return vehicleFilters[0] if vehicleFilters else None
	
	def onVehicleStart(self, vehicleID):
		if vehicleID not in self.__visibleVehicles:
			self.__visibleVehicles.add(vehicleID)
			self.__updateMarkers()
		return None
	
	def onVehicleStop(self, vehicleID):
		if vehicleID in self.__visibleVehicles:
			self.__visibleVehicles.remove(vehicleID)
			self.__updateMarkers()
		return None
	
	def onVehicleDeath(self, vehicleID):
		if vehicleID in self.__visibleVehicles:
			self.__visibleVehicles.remove(vehicleID)
			self.__updateMarkers()
		return None
	
	def onVehicleFiltersChanged(self):
		self.__updateMarkers()
		return None
	
	def __addVehicleGunMarker(self, vehicleID):
		vehicle = BigWorld.entity(vehicleID)
		if vehicle is not None:
			zIndex = self.getMinimap().zIndexManager.getGunIndex(vehicleID)
			combinedMP = self.getWGCombinedMP(vehicle.matrix, vehicle.appearance.modelsDesc['gun']['model'].matrix)
			self.__minimapEntryManager.addEntry(vehicleID, combinedMP, zIndex)
			self.__minimapEntryManager.getEntry(vehicleID).invoke('gotoAndStop', ['cursorNormal'])
		return None
	
	def __delVehicleGunMarker(self, vehicleID):
		self.__minimapEntryManager.delEntry(vehicleID)
		return None
	
	def __updateMarkers(self):
		if not self.__enabled:
			return self.__minimapEntryManager.clear()
		vehiclesNew = set(sum([filter(vehicleFilter, self.__visibleVehicles) for vehicleFilter in self.__vehicleFilters], []))
		vehiclesOld = set(self.__minimapEntryManager.entries.keys())
		map(self.__delVehicleGunMarker, vehiclesOld - vehiclesNew)
		map(self.__addVehicleGunMarker, vehiclesNew - vehiclesOld)
		return None
	
	def __del__(self):
		return None
