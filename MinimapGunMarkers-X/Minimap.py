class MinimapEntry(object):
	@staticmethod
	def getMinimapOwnUI():
		from gui.WindowsManager import g_windowsManager
		return g_windowsManager.battleWindow.minimap._Minimap__ownUI
	
	def __init__(self, matrixProvider, zIndex):
		self.__handle = self.getMinimapOwnUI().addEntry(matrixProvider, zIndex)
		return None
	
	def invoke(self, funcName, argsList = None):
		return self.getMinimapOwnUI().entryInvoke(self.__handle, (funcName, argsList))
	
	def setMatrixProvider(self, matrixProvider):
		return self.getMinimapOwnUI().entrySetMatrix(self.__handle, matrixProvider)
	
	def __del__(self):
		return self.getMinimapOwnUI().delEntry(self.__handle)

class MinimapEntryManager(object):
	def __init__(self):
		self.__entries = {}
		return None
	
	@property
	def entries(self):
		return self.__entries
	
	def hasEntry(self, key):
		return key in self.__entries.keys()
	
	def addEntry(self, key, matrixProvider, zIndex):
		self.__entries[key] = MinimapEntry(matrixProvider, zIndex)
		return None
	
	def delEntry(self, key):
		del self.__entries[key]
		return None
	
	def getEntry(self, key):
		return self.__entries[key] if self.hasEntry(key) else None
	
	def clear(self):
		return self.__entries.clear()
	
	def __del__(self):
		return None
